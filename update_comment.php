<?php

session_start();

$title = "Изменение комментария";
require "blocks\header.php";
require "CRUD.php";
$result = CRUD::readCommentBYID($_POST["id"] ?? $_SESSION["id"]);
?>
<div class="container mt-2">
    <h2>Изменить комментарий</h2>
    <form action="controler.php" method="post">
        <input type="hidden" name="name_route" value="update">
        <input type="hidden" name="id" value="<?php echo $result['id'] ?>">
        <input type="text" name="username" value="<?php echo $result['name'] ?? '' ?>" placeholder="Введите имя"
               class="form-control">
        <?php if (isset($_SESSION['ErrorToShortName'])): ?>
            <div class="text-danger"><?= $_SESSION['ErrorToShortName'] ?></div>
        <?php endif; ?>
        <br>
        <?php if (isset($_SESSION['ErrorNoName'])): ?>
            <div class="text-danger"><?= $_SESSION['ErrorNoName'] ?></div>
        <?php endif; ?>
        <br>

        <input type="text" name="email" value="<?php echo $result['email'] ?? '' ?>" placeholder="Введите email"
               class="form-control">
        <?php if (isset($_SESSION['ErrorNoEmail'])): ?>
            <div class="text-danger"><?= $_SESSION['ErrorNoEmail'] ?></div>
        <?php endif; ?>
        <br>
        <?php if (isset($_SESSION['ErrorInvalidEmail'])): ?>
            <div class="text-danger"><?= $_SESSION['ErrorInvalidEmail'] ?></div>
        <?php endif; ?>
        <br>

        <textarea name="message" placeholder="Введите сообщение"
                  class="form-control"><?php echo $result['message'] ?? '' ?></textarea>

        <?php if (isset($_SESSION['ErrorToShortMessage'])): ?>
            <div class="text-danger"><?= $_SESSION['ErrorToShortMessage'] ?></div>
        <?php endif; ?>
        <br>
        <button type="submit" class="btn btn-success">Изменить комментарий</button>
    </form>
    <p class="card-text"><small class="text-muted">Создано:  <?php echo $result['created']?></small></p>
    <p class="card-text"><small class="text-muted">Обновлено:  <?php echo $result['updated']?></small></p>
</div>
<div>