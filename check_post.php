<?php
    session_start();

    require "CRUD.php";

    unset($_SESSION['username']);
    unset($_SESSION['email']);
    unset($_SESSION['message']);
    $_SESSION['ErrorToShortName'] = '';
    $_SESSION['ErrorNoName'] = '';
    $_SESSION['ErrorNoEmail'] = '';
    $_SESSION['ErrorInvalidEmail'] = '';
    $_SESSION['ErrorToShortMessage'] = '';
    $name = trim($_POST['username']);
    $email = trim($_POST['email']);
    $message = trim($_POST['message']);

    $_SESSION['username'] = $name;
    $_SESSION['email'] = $email;
    $_SESSION['message'] = $message;

function redirect()
    {
    header("location:index.php");
    exit;
    }
    if(trim($name) == "")
    {
        $_SESSION['ErrorNoName'] = "Вы не ввели имя";
        redirect();
    }
    else if(strlen(trim($name)) <=1)
    {
        $_SESSION['ErrorToShortName'] = "Имя слишком короткое";
        redirect();
    }
    else if(trim($email) == "")
    {
        $_SESSION['ErrorNoEmail'] = "Вы не ввели email";
        redirect();
    }
    else if(strlen($email) < 5 || !strpos($email, "@"))
    {

        $_SESSION['ErrorInvalidEmail'] = "Неккоректная форма записи email";

        redirect();
    }
    else if(strlen(trim(($message)) < 4 ))
    {
        $_SESSION['ErrorToShortMessage'] = "Комментарий не может быть короче 3 символов";
    }
    else
    {
        $mysql->query("INSERT INTO `Comments` (`name`, `email`, `message`) VALUES('$name', '$email', '$message')");
    }
        header('Location:index.php');
        exit;
