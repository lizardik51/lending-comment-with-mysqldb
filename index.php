<?php
session_start();

$title = "Какая-то одностраничка";
require "blocks\header.php";
require "CRUD.php";

?>
<div class="container mt-2">
    <h2>Оставить комментарий</h2>
    <form action="Controler.php" method="post">
        <input type="hidden" name="name_route" value="create">
        <input type="text" name="username" value="<?php echo $_SESSION['username'] ?? '' ?>" placeholder="Введите имя"
               class="form-control">
        <?php if (isset($_SESSION['ErrorToShortName'])): ?>
            <div class="text-danger"><?= $_SESSION['ErrorToShortName'] ?></div>
        <?php endif; ?>
        <br>
        <?php if (isset($_SESSION['ErrorNoName'])): ?>
            <div class="text-danger"><?= $_SESSION['ErrorNoName'] ?></div>
        <?php endif; ?>
        <br>

        <input type="text" name="email" value="<?php echo $_SESSION['email'] ?? '' ?>" placeholder="Введите email"
               class="form-control">
        <?php if (isset($_SESSION['ErrorNoEmail'])): ?>
            <div class="text-danger"><?= $_SESSION['ErrorNoEmail'] ?></div>
        <?php endif; ?>
        <br>
        <?php if (isset($_SESSION['ErrorInvalidEmail'])): ?>
            <div class="text-danger"><?= $_SESSION['ErrorInvalidEmail'] ?></div>
        <?php endif; ?>
        <br>

        <textarea name="message" placeholder="Введите сообщение"
                  class="form-control"><?php echo $_SESSION['message'] ?? '' ?></textarea>

        <?php if (isset($_SESSION['ErrorToShortMessage'])): ?>
            <div class="text-danger"><?= $_SESSION['ErrorToShortMessage'] ?></div>
        <?php endif; ?>
        <br>
        <input type="submit" value="Отправить комментарий" class="btn btn-success">
    </form>
</div>
<div>
    <h3>Комментарии:</h3>
</div>
<a href="?sortBy=name&sortOrder=ASC">Сортировать по имени (по возрастанию)</a>
<a href="?sortBy=name&sortOrder=DESC">Сортировать по имени (по убыванию)</a><br>
<a href="?sortBy=created&sortOrder=ASC">Сортировать по дате создания (по возрастанию)</a>
<a href="?sortBy=created&sortOrder=DESC">Сортировать по дате создания (по убыванию)</a><br>
<a href="?sortBy=updated&sortOrder=ASC">Сортировать по дате изменения (по возрастанию)</a>
<a href="?sortBy=updated&sortOrder=DESC">Сортировать по дате изменения (по убыванию)</a>
<?php
$sortBy = $_GET['sortBy'] ?? 'created';
$sortOrder = $_GET['sortOrder'] ?? 'DESC';
$result = CRUD::readComments($sortBy, $sortOrder);
foreach ($result as $row) {
    echo '<div class="card mb-3">';
    echo '<div class="card-body">';
    echo '<h5 class="card-title">' . htmlspecialchars($row['name']) . '</h5>';
    echo '<p class="fs-4"" >' . htmlspecialchars($row['message']) . '</p>';
    echo '<p class="card-text"><small class="text-muted">' . htmlspecialchars($row['email']) . '</small></p>';
    echo '<p class="card-text"><small class="text-muted">Создано: ' . $row['created'] . '</small></p>';
    echo '<p class="card-text"><small class="text-muted">Обновлено: ' . $row['updated'] . '</small></p>';
    echo '<form action="controler.php" method="post">';
    echo '<input type="hidden" name="name_route" value="delete">';
    echo '<input type="hidden" name="id" value="' . $row['id'] . '">';
    echo '<button type="submit" class="btn btn-outline-danger">Удалить</button>';
    echo '</form>';
    echo '<br>';
    echo '<form action="update_comment.php" method="post">';
    echo '<input type="hidden" name="id" value="' . $row['id'] . '">';
    echo '<button type="submit" class="btn btn-outline-primary">Изменить</button>';
    echo '</form>';
    echo '</div>';
    echo '</div>';

}
require "blocks/footer.php";
?>
