<?php

use JetBrains\PhpStorm\NoReturn;

require_once "CRUD.php";
require_once "validation.php";
use Validation\Validator;
$validator = new validator();
global $validator;
session_start();
function redirect(): void
{
    header("location:index.php");
    exit;
}
function redirectUPDATE(): void
{
    $_SESSION['id'] = $_POST['id'];
    header("location:update_comment.php");
    exit;
}
function dropErrors():void
{
    $_SESSION['ErrorToShortName'] = '';
    $_SESSION['ErrorNoName'] = '';
    $_SESSION['ErrorNoEmail'] = '';
    $_SESSION['ErrorInvalidEmail'] = '';
    $_SESSION['ErrorToShortMessage'] = '';
}

if (isset($_POST['name_route']) && function_exists($_POST['name_route'])) {
    call_user_func($_POST['name_route'], $_POST);
}
else
    echo "Error: No call route";
function create(array $data)
{
    global $validator;
    dropErrors();
    $data = [
        'id' => $_POST['id'],
        'username' => $_POST['username'],
        'email' => $_POST['email'],
        'message' => $_POST['message']];
    if ($validator->validate($data))
    {
        CRUD::createComment($_POST['username'],$_POST['email'], $_POST['message']);
        dropErrors();
        redirect();
    }
    else
        redirect();
}
function update(array $data):void
{
    global $validator;
    dropErrors();
    $data = [
            'id' => $_POST['id'],
            'username' => $_POST['username'],
            'email' => $_POST['email'],
            'message' => $_POST['message']
    ];
    if ($validator->validate($data))
    {   CRUD::updateComment($_POST['id'],$_POST['username'], $_POST['email'], $_POST['message']);
        dropErrors();
        redirect();
    }
    else
        redirectUPDATE();

}
function delete(array $data): void
{
    if (isset($_POST['id']) && is_numeric($_POST['id']))
        CRUD::deleteComment($_POST['id']);

    redirect();
}





