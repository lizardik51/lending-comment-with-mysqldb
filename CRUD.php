<?php
session_start();
require_once 'db.php';
use DBServise\Db;
$db = new Db();
global $db;
Class CRUD
{
    private static Db $db;
    public static function createComment($name, $email, $message): void
    {
        global $db;
        $params = [':name' => $name, ':email' => $email, ':message' => $message];
        $result = $db->query("INSERT INTO comments (name, email, message) VALUES (:name, :email, :message)", $params);

    }
    public static function readComments($sortBy, $sortOrder) :array
    {
        global $db;
        $result = $db->query("SELECT * FROM `Comments` ORDER BY `$sortBy` $sortOrder");
        return $result;
    }
    public static function readCommentBYID($id) : array
    {
        if(isset($id) && is_numeric($id))
        {
            global $db;
            $result = $db->query("SELECT * FROM `Comments` WHERE `id` = '$id'");
            return $result[0];
        }
        else
        {
            echo 'Error: no id';
            die;
        }
    }

    public static function updateComment($id, $name, $email, $message):void
    {
        global $db;
        $params = [':id' => $id,':name' => $name, ':email' => $email, ':message' => $message];
        $sql = "UPDATE `Comments` SET `name` = :name, `email` = :email, `message` = :message, `updated` = NOW() WHERE `id` = :id";
        $request = $db->query($sql,$params);

    }
    public static function deleteComment($id)
    {
        global $db;
        $sql = "DELETE FROM `Comments` WHERE `id` = '$id'";
        $request = $db->query($sql);
    }
}
